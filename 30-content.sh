#!/bin/bash

set -e

source 01-common.sh

figlet STL
echo

OUTDIR=website/content/part

echo -n "Creating dirs ... "
for TYPE in ${TYPES[@]}; do
    mkdir -p $OUTDIR/$TYPE
done
echo done
echo

echo -n "Removing old files ... "
for TYPE in ${TYPES[@]}; do
    rm -f $OUTDIR/$TYPE/*.md
done
echo done
echo

echo "Generating Markdown files:"
echo
for FILE in ${PARTS[@]}; do
    SCADFILE="src/${FILE}.scad"
    MDFILE="$OUTDIR/${FILE}.md"
    echo "* $SCADFILE -> $MDFILE"
    grep '//+' $SCADFILE | cut -c 5- > $MDFILE
done
echo

echo "Finished"
