#!/bin/bash

set -e

figlet Deploy
echo

# deploy from `website`
cd website

# deploy
netlify deploy --prod --dir=dist

# go back to previous dir
cd -
