#!/bin/bash

set -e

figlet Lint
echo

source 01-common.sh

for FILE in ${PARTS[@]}; do
    # check that each scad file exists
    FILENAME="src/${FILE}.scad"
    echo "* $FILENAME"
    if [ ! -f "$FILENAME" ]; then
        echo "File $FILENAME missing!"
        exit 2
    fi
done

for FILE in ${PARTS[@]}; do
    # check that each part also has a website page
    FILENAME="website/content/part/${FILE}.md"
    echo "* $FILENAME"
    if [ ! -f "$FILENAME" ]; then
        echo "File $FILENAME missing!"
        exit 2
    fi

    # Check each web page isn't empty. If so, we forgot the //+ content in the
    # source file)
    if [ ! -s "$FILENAME" ]; then
        echo "File $FILENAME empty (0 bytes)!"
        exit 2
    fi
done

echo
echo "Finished"
