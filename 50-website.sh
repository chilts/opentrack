#!/bin/bash

set -e

figlet Website
echo

# build and deploy from `website`
cd website

# build
seagull build

# go back to previous dir
cd -

echo
echo "Finished"
