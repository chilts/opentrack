TYPES=(
    track
    double
    opentrack-x
    road
)

PARTS=(
    track/straight-micro

    track/straight-mini-variant-a
    track/straight-mini-variant-b
    track/straight-mini-variant-c

    track/straight-short-variant-a
    track/straight-short-variant-b
    track/straight-short-variant-c

    track/straight-medium

    track/straight-middle

    track/straight-long

    track/curve-short
    track/curve-long

    track/switch-curve-long-variant-a
    track/switch-curve-long-variant-b
    track/switch-curve-short-variant-a
    track/switch-curve-short-variant-b
    track/switch-t-variant-a
    track/switch-t-variant-b
    track/switch-variant-a
    track/switch-variant-b

    track/turnout-long-variant-a
    track/turnout-long-variant-b
    track/turnout-long-x2-variant-a
    track/turnout-long-x2-variant-b
    track/turnout-long-x4-variant-a
    track/turnout-long-x4-variant-b

    track/cross
    track/crossing
    track/criss-cross-variant-a
    track/criss-cross-variant-b
    track/cross-curve-variant-a
    track/cross-curve-variant-b

    track/bone

    double/double-straight-mini
    double/double-straight-short
    double/double-straight-middle
    double/double-straight-medium
    double/double-straight-long
    double/switch-parallel-2-way-variant-a
    double/switch-parallel-2-way-variant-b
    double/switch-parallel-3-way-variant-a
    double/switch-parallel-3-way-variant-b
    double/switch-parallel-2-splitter-variant-a
    double/switch-parallel-2-splitter-variant-b

    opentrack-x/wonky-medium
    opentrack-x/triple-switch-variant-a
    opentrack-x/triple-switch-variant-b

    road/a2-road-only
    road/a1-road
)
