#!/bin/bash

set -e

source 01-common.sh

figlet Images
echo

OUTDIR=website/static/s/img

echo -n "Creating dirs ... "
for TYPE in ${TYPES[@]}; do
    mkdir -p $OUTDIR/$TYPE
done
echo done
echo

echo -n "Removing old files ... "
for TYPE in ${TYPES[@]}; do
    rm -f $OUTDIR/$TYPE/*.png
done
echo done
echo

echo "Generating images:"
for FILE in ${PARTS[@]}; do
  echo "* src/${FILE}.scad -> $OUTDIR/${FILE}.png"
  openscad \
    --autocenter \
    --camera 0.00,8.00,0.00,55.00,0.00,25.00,500 \
    --render 1 \
    --imgsize 640,480 \
    --hardwarnings \
    --colorscheme Starnight \
    -o $OUTDIR/${FILE}.png \
    src/${FILE}.scad
done
echo

for TYPE in ${TYPES[@]}; do
  echo -n "Generating $TYPE tessellation ..."
  montage \
    $OUTDIR/$TYPE/*.png \
    -tile 6x6 \
    -geometry +0+0 \
    -border 10 \
    -bordercolor white \
    $OUTDIR/$TYPE/all.png
  echo Done
done

echo
echo "Finished"
