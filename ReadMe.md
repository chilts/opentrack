# Open Track #

A 3D printable track compatible with wooden toy tracks such as Thomas, Brio,
Hape, Melissa and Doug and other generic brands.

* https://opentrack.xyz

This repo contains the specification and implementation of every (major) wooden
track part.

Pull requests for new parts will generally be accepted. :) Many thanks.

This collection of track is designed to fit with your current wooden track
collection. It is:

1. compatible with all major brands (see below)
1. extremely comprehensive
1. extensible with your own designs

I've seen various bits of track and collections on 3D/STL repositories, but
this collection will surpass all of those.

## Background ##

I (Andy Chilton) decided to create this because my son is very much a train
boy. We have built and played with many tracks over the past 4 years and it
seemed like a great idea to build and expand the track with a 3D printer. After
creating my first piece in TinkerCad and printing it at the local library, it
was time to get serious with OpenSCAD and my own 3D printer.

## The Collection ##

ToDo.

## Creating your own Track ##

This collection of OpenSCAD files can help you create your own unique track to
print for yourself. We have modules to be able to help you create:

* straight track of any length
* curved track of both diameters
* tenon (male) or mortisse (female) connectors at either end
* rails, road, or no track on both upper or lower sides
* square, round, or triangular profiles for the track's rails

And of course you have the ability to change general dimensions in the
`config.scad` file to your own liking (such as 40mm or 42mm track width).

## Compatible Brands ##

* Thomas (Guillaume) Ltd
* Brio
* Hape
* Melissa and Doug
* Ikea
* Asda George
* Other Generic Wooden Track

## Inspiration ##

After stumbling upon these projects and parts, I figured I wanted to go a bit further:

* (Small collection of wooden train track compatible
pieces)[https://www.thingiverse.com/thing:1938409]
* (Wagon and container for wooden train)[https://www.thingiverse.com/thing:3429286]
* (Train Tracks Complete Set (28 pieces))[https://www.thingiverse.com/thing:611316]
* (3x switch for wooden railroad)[https://www.thingiverse.com/thing:15166]
* (wooden train connector)[https://www.thingiverse.com/thing:15165]
* ([Bridge for Brio / IKEA Lillabo wooden train tracks](https://www.thingiverse.com/thing:1897129)

## Author ##

Andy Chilton:

* andychilton@gmail.com
* https://chilts.org/

## License ##

OpenTrack by Andrew Chilton is licensed under the Creative Commons
Attribution-ShareAlike 4.0 International License. Based on a work at
[OpenTrack](https://codeberg.org/chilts/opentrack).

To view a copy of this license, visit
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) or send a
letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

(Ends)
