#!/bin/bash

set -e

./10-stl.sh
./20-images.sh
./30-content.sh
./40-lint.sh
./50-website.sh
./60-deploy.sh
