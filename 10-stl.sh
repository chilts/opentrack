#!/bin/bash

set -e

source 01-common.sh

figlet STL
echo

OUTDIR=website/static/s/stl

echo -n "Creating dirs ... "
for TYPE in ${TYPES[@]}; do
    mkdir -p $OUTDIR/$TYPE
done
echo done
echo

echo -n "Removing old files ... "
for TYPE in ${TYPES[@]}; do
    rm -f $OUTDIR/$TYPE/*.stl
done
echo done
echo

echo "Building STL files:"
echo
for FILE in ${PARTS[@]}; do
  echo "* src/${FILE}.scad -> $OUTDIR/${FILE}.stl"
  openscad \
    --hardwarnings \
    -o $OUTDIR/${FILE}.stl \
    src/${FILE}.scad
done
echo

echo
echo "Finished"
