//+ {
//+   "name": "switch-parallel-3-way-variant-a",
//+   "title": "Parallel Switch 3 Way (Variant A)",
//+   "type": "part",
//+   "brio": "F2",
//+   "specs": {
//+     "type": "Switch",
//+     "double": true,
//+     "length": "144",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-05-03T11:27:37.466Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <./switch-parallel-base.scad>;

module SwitchParallel3WayVariantA() {
  SwitchParallelBase(
    aLength,
    "both",
    step = 1,
    top = "track",
    bottom = "blank",
    connector1 = "mortise",
    connector2 = "tenon"
  );
}

SwitchParallel3WayVariantA();
