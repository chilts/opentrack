//+ {
//+   "name": "switch-parallel-2-splitter-variant-b",
//+   "title": "Parallel Switch 2 Splitter (Variant B)",
//+   "type": "part",
//+   "specs": {
//+     "type": "Switch",
//+     "double": true,
//+     "length": "144",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "tenon",
//+     "connector2": "mortise"
//+   },
//+   "published": "2020-06-21T04:33:33.951Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <./switch-parallel-base.scad>;

module SwitchParallel2SplitterVariantB() {
  SwitchParallelBase(
    aLength,
    "split",
    step = 1,
    top = "track",
    bottom = "blank",
    connector1 = "tenon",
    connector2 = "mortise"
  );
}

SwitchParallel2SplitterVariantB();
