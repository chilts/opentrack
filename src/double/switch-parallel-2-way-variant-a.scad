//+ {
//+   "name": "switch-parallel-2-way-variant-a",
//+   "title": "Parallel Switch 2 Way (Variant A)",
//+   "type": "part",
//+   "brio": "F1",
//+   "specs": {
//+     "type": "Switch",
//+     "double": true,
//+     "length": "144",
//+     "top": "track",
//+     "bottom": "track",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-05-03T11:27:37.466Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <./switch-parallel-base.scad>;

module SwitchParallel2WayVariantA() {
  SwitchParallelBase(
    aLength,
    "left",
    step = 1,
    top = "track",
    bottom = "track",
    connector1 = "mortise",
    connector2 = "tenon"
  );
}

SwitchParallel2WayVariantA();
