//+ {
//+   "name": "double-straight-mini",
//+   "title": "Double Mini Straight (54mm)",
//+   "type": "part",
//+   "specs": {
//+     "type": "Straight",
//+     "double": true,
//+     "length": "54",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-03-27T22:19:31.049Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

module DoubleStraightMini() {
  straightDouble(unitTrackLength);
}

DoubleStraightMini();
