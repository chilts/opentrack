//+ {
//+   "name": "switch-parallel-2-splitter-variant-a",
//+   "title": "Parallel Switch 2 Splitter (Variant A)",
//+   "type": "part",
//+   "specs": {
//+     "type": "Switch",
//+     "double": true,
//+     "length": "144",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-06-21T04:33:33.951Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <./switch-parallel-base.scad>;

module SwitchParallel2SplitterVariantA() {
  SwitchParallelBase(
    aLength,
    "split",
    step = 1,
    top = "track",
    bottom = "blank",
    connector1 = "mortise",
    connector2 = "tenon"
  );
}

SwitchParallel2SplitterVariantA();
