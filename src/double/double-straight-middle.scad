//+ {
//+   "name": "double-straight-middle",
//+   "title": "Double Mini Middle (72mm)",
//+   "type": "part",
//+   "specs": {
//+     "type": "Straight",
//+     "double": true,
//+     "length": "72",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-07-04T23:54:44.457Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

module DoubleStraightMiddle() {
  straightDouble(a3Length);
}

DoubleStraightMiddle();
