include <../config.scad>;
use <../lib/connector.scad>;
use <../lib/track.scad>;
use <../lib/make-parallel-curve.scad>;

offset = doubleWidthOffsetFromCentre;

module SwitchParallelBase(
  length,
  switch,
  step = 0.5,
  style = defaultStyle,
  top = "track",
  bottom = "blank",
  connector1 = "tenon",
  connector2 = "mortise"
  ) {
  union() {
    difference() {
      union() {
        if ( switch != "split" ) {
          makeStraight(length)
            track2d(style);
        }
        if ( switch == "left" || switch == "both" ) {
          makeParallelCurve(length, offset, step)
            track2d(style, top, bottom);
        }
        if ( switch == "right" || switch == "both" ) {
          makeParallelCurve(length, -offset, step)
            track2d(style, top, bottom);
        }
        if ( switch == "split" ) {
          makeParallelCurve(length, offset/2, step)
            track2d(style, top, bottom);
          makeParallelCurve(length, -offset/2, step)
            track2d(style, top, bottom);
        }
      }

      // take off any mortise
      if (connector1 == "mortise") {
        translate([0, 0 - length/2 + mortiseTotalLength/2, 0])
          connector("mortise", top, bottom);
      }
      if (connector2 == "mortise") {
        if ( switch != "split" ) {
          translate([0, 0 + length/2 - mortiseTotalLength/2, 0])
            mirror([0, 1, 0])
              connector("mortise", top, bottom);
        }
        if ( switch == "left" || switch == "both" ) {
          translate([-offset*2, 0 + length/2 - mortiseTotalLength/2, 0])
            mirror([0, 1, 0])
              connector("mortise", top, bottom);
        }
        if ( switch == "right" || switch == "both" ) {
          translate([offset*2, 0 + length/2 - mortiseTotalLength/2, 0])
            mirror([0, 1, 0])
              connector("mortise", top, bottom);
        }
        if ( switch == "split" ) {
          translate([-offset, 0 + length/2 - mortiseTotalLength/2, 0])
            mirror([0, 1, 0])
              connector("mortise", top, bottom);
          translate([offset, 0 + length/2 - mortiseTotalLength/2, 0])
            mirror([0, 1, 0])
              connector("mortise", top, bottom);
        }
      }

      // take off the rails
      if ( switch != "split" ) {
        makeStraight(length)
          insets2d(style, top, bottom);
      }
      if ( switch == "left" || switch == "both" ) {
        makeParallelCurve(length, offset, step)
          insets2d(style, top, bottom);
      }
      if ( switch == "right" || switch == "both" ) {
        makeParallelCurve(length, -offset, step)
          insets2d(style, top, bottom);
      }
      if ( switch == "split" ) {
        makeParallelCurve(length, offset/2, step)
          insets2d(style, top, bottom);
        makeParallelCurve(length, -offset/2, step)
          insets2d(style, top, bottom);
      }
    }

    if (connector1 == "tenon") {
      translate([0, 0 - length/2 - tenonTotalLength/2, 0])
        connector("tenon", top, bottom);
    }

    if (connector2 == "tenon") {
      if ( switch != "split" ) {
        translate([0, 0 + length/2 + tenonTotalLength/2, 0])
          mirror([0, 1, 0])
            connector("tenon", top, bottom);
      }
      if ( switch == "left" || switch == "both" ) {
        translate([-offset*2, 0 + length/2 + tenonTotalLength/2, 0])
          mirror([0, 1, 0])
            connector("tenon", top, bottom);
      }
      if ( switch == "right" || switch == "both" ) {
        translate([offset*2, 0 + length/2 + tenonTotalLength/2, 0])
          mirror([0, 1, 0])
            connector("tenon", top, bottom);
      }
      if ( switch == "split" ) {
        translate([-offset, 0 + length/2 + tenonTotalLength/2, 0])
          mirror([0, 1, 0])
            connector("tenon", top, bottom);
        translate([offset, 0 + length/2 + tenonTotalLength/2, 0])
          mirror([0, 1, 0])
            connector("tenon", top, bottom);
      }
    }
  }
}
