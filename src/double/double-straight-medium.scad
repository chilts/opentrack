//+ {
//+   "name": "double-straight-medium",
//+   "title": "Double Mini Medium (144mm)",
//+   "type": "part",
//+   "specs": {
//+     "type": "Straight",
//+     "double": true,
//+     "length": "144",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-07-04T23:54:44.457Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

module DoubleStraightMedium() {
  straightDouble(aLength);
}

DoubleStraightMedium();
