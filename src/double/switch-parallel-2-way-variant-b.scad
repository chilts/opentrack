//+ {
//+   "name": "switch-parallel-2-way-variant-b",
//+   "title": "Parallel Switch 2 Way (Variant B)",
//+   "type": "part",
//+   "brio": "G1",
//+   "specs": {
//+     "type": "Switch",
//+     "double": true,
//+     "length": "144",
//+     "top": "track",
//+     "bottom": "track",
//+     "connector1": "tenon",
//+     "connector2": "mortise"
//+   },
//+   "published": "2020-05-03T11:27:37.466Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <./switch-parallel-base.scad>;

module SwitchParallel2WayVariantB() {
  SwitchParallelBase(
    aLength,
    "left",
    step = 1,
    top = "track",
    bottom = "track",
    connector1 = "tenon",
    connector2 = "mortise"
  );
}

SwitchParallel2WayVariantB();
