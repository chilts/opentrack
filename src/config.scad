// ----------------------------------------------------------------------------
//
// Regular Wooden Train Track (sourced from measurements on Thomas, Brio, Hape, etc)
//
// Track types:
//
// * https://woodenrailway.info/track/brio-track-guide
// * https://woodenrailway.info/wp-content/uploads/2018/05/the-modern-brio-track-id-chart-e1527529416190.bmp
// * https://woodenrailway.info/wp-content/uploads/2018/05/track-id-chart-with-double-track-237x300.bmp
//
// ----------------------------------------------------------------------------
//
// A typical piece of BRIO track is 40mm wide and 12mm high, with two 6mm wide
// grooves that are 3mm deep, spaced 20mm // apart, measuring 26mm from center
// to center. The tenon connector is an 11.5mm peg on a 7mm long neck, and the
// mortise connector ranges from 15 to 17mm in diameter with a 5mm long throat.
//
// Double track is spaced 46mm from center to center, making it 6mm wider than
// two track pieces placed side-by-side.
//
// ----------------------------------------------------------------------------

// OpenSCAD
$fn = $preview ? 18 : 36;

// OpenSCAD
epsilon = 0.01;

// generic
unitTrackLength = 54; // our own "1 Unit of Track Length"
trackWidth = 40;
trackHeight = 12;
doubleWidthSpacer = 10;
doubleWidth = 2 * trackWidth + doubleWidthSpacer;
doubleWidthOffsetFromCentre = trackWidth / 2 + doubleWidthSpacer / 2;

// rail(s)
railWidth = 6;
railDepth = 3;
railDiff = 20 + 1 * railWidth; // from centre to centre
railDistFromEdge = ( trackWidth - 20 - 2 * railWidth ) / 2;

// road
roadWidth = 32;
roadDepth = 3;
roadDistFromEdge = ( trackWidth - roadWidth ) / 2;
roadMarkingWidth = 3;
roadMarkingLength = 15; // \_ Note: these two add up to 27, which is
roadMarkingSpacer = 12; // /  a multiple of `a2Length`.
roadMarkingRepeat = roadMarkingLength + roadMarkingSpacer;
roadMarkingHeight = 1.0;
roadMarkingBevel = 0.25;

// bevels
roundBevel = 3;
triangleBevel = 2;

// tenon
tenonNeckWidth = 6.5;
tenonDiameter = 11.5;
tenonTotalLength = 17.5;
tenonNeckLength = tenonTotalLength - tenonDiameter / 2;

// mortise
mortiseNeckWidth = 7.5;
mortiseDiameter = 12.5;
mortiseTotalLength = 17.5;
mortiseNeckLength = mortiseTotalLength - mortiseDiameter / 2;

// straight track:
// * https://woodenrailway.info/track/brio-track-guide)
// * https://woodenrailway.info/wp-content/uploads/2018/05/the-modern-brio-track-id-chart-e1527529416190.bmp
dLength = 216;
aLength = dLength * 2 / 3; // 144 (2/3 x D)
a1Length = dLength / 2; // 108 (1/2 x D)
a2Length = dLength / 4; // 54 (1/4 x D)
a3Length = dLength / 3; // 72 (1/3 x D) or (1/2 x A)
microLength = a2Length / 2; // 27 (1/8 x D)

// curved track
defaultAngle = 45;

// curved track (large radius)
//
// Inside radius is about 182, so add half of it's own width on (since it is centered there), and remove the distance to the edge from the rail.
eRadius = 182 + trackWidth/2 - railDistFromEdge;

// curved track (large radius)
//
// Inside radius is about 90, so add half of it's own width on (since it is centered there), and remove the distance to the edge from the rail.
e1Radius = 90 + trackWidth/2 - railDistFromEdge;

// T track is 2mm tighter than E1.
tRadius = 88 + trackWidth/2 - railDistFromEdge;
tAngle = 90;

// ----------------------------------------------------------------------------
// user options

defaultStyle = "round";

// ----------------------------------------------------------------------------
