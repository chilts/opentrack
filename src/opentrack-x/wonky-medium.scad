//+ {
//+   "name": "wonky-medium",
//+   "title": "Medium Wonky",
//+   "type": "part",
//+   "specs": {
//+     "type": "Wonky",
//+     "length": "144",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-05-02T11:33:14.919Z"
//+ }
//+
//+ ---
//+

// Note to the Reader.
//
// I'm really proud I managed to get this to actually work! However, when
// sliced in your slicer, every little segment is (of course) adding to the head
// being quite jittery and making the print slow, instead of every curve being
// nice and smooth.
//
// Can anyone suggest a better way to do this? I thought I could try multmatrix()
// but I obviously don't understand it enough. :)
//
// Many thanks.
// Chilts.

include <../config.scad>;
use <../lib/connector.scad>;
use <../lib/track.scad>;

// step = 1.0;
// step = 0.5;
// step = 0.25;
step = 0.2;
// step = 0.1;

// We tried this as 5, but it was too much so trying 3.
amplitude = 3;

function tx(i, length) = (i < (-length/2+20)) ? 0 : (i > length/2-20) ? 0 : sin((length/2+i-20)/(length-40) * (360*2)) * amplitude;

module Wonky(
  length,
  style = defaultStyle,
  top = "track",
  bottom = "blank",
  connector1 = "tenon",
  connector2 = "mortise"
  ) {
  union() {
    difference() {
      union() {
        for(i = [-length/2 : step : length/2]) {
          // echo(tx(i, length));
          translate([tx(i, length), i, 0])
            rotate([90, 0, 0])
              linear_extrude(height = step, center = false)
                track2d(style, "track");
        }
      }

      if (connector1 == "mortise") {
        translate([0, 0 + length/2 - mortiseTotalLength/2, 0])
          mirror([0, 1, 0])
            connector("mortise", top, bottom);
      }

      if (connector2 == "mortise") {
        translate([0, 0 - length/2 + mortiseTotalLength/2, 0])
          connector("mortise", top, bottom);
      }
    }

    if (connector1 == "tenon") {
      translate([0, 0 + length/2 + tenonTotalLength/2, 0])
        mirror([0, 1, 0])
          connector("tenon", top, bottom);
    }

    if (connector2 == "tenon") {
      translate([0, 0 - length/2 - tenonTotalLength/2, 0])
        connector("tenon", , top, bottom);
    }
  }
}

// aLength = Middle Track (144mm)
Wonky(aLength);
