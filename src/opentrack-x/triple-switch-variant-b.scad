//+ {
//+   "name": "triple-switch-variant-a",
//+   "title": "Triple Switch (Variant A)",
//+   "type": "part",
//+   "specs": {
//+     "type": "Junction",
//+     "radius": "90",
//+     "angle": "45",
//+     "top": "Track",
//+     "bottom": "Blank",
//+     "connector1": "tenon"
//+   },
//+   "published": "2020-04-29T11:08:12.628Z"
//+ }
//+
//+ ---
//+

use <./triple-switch-base.scad>;

module TripleSwitchVariantB() {
  tripleSwitchBase(connector = "tenon");
}

TripleSwitchVariantB();
