include <../config.scad>;
use <../lib/track.scad>;
use <../lib/curved-base.scad>;
use <../lib/curved-track.scad>;

// 3 tracks, so 180 / 3 = 60deg each.
angle = 60;

// To centre the piece I dunno how I figured out this magic number. Probably
// some equation I could use to get the exact amount.
adjustment = 16.39;

module tripleSwitchBase(connector) {
  difference() {
    union() {
      // union all of this, and take off the tracks
      for(a = [0 : 120 : 259]) {
        rotate([0, 0, a]) {
          translate([-e1Radius - adjustment, 0, 0]) {
            rotate([0, 0, -angle/2]) {
              curvedTrack(
                e1Radius,
                connector1 = connector,
                connector2 = connector,
                angle = angle
              );
            }
          }
        }
      }
    }

    // take off all tracks
    for(a = [0 : 120 : 259]) {
      rotate([0, 0, a]) {
        translate([-e1Radius - adjustment, 0, 0]) {
          rotate([0, 0, -angle/2]) {
            makeCurved(e1Radius, angle = angle)
              insets2d(defaultStyle, "track", "blank");
          }
        }
      }
    }
  }
}
