include <config.scad>;
include <lib/straight-track.scad>
include <lib/curved-track.scad>

use <part/a.scad>;
use <part/a1.scad>;
use <part/a2.scad>;
use <part/a3.scad>;
use <part/b2.scad>;
use <part/c2.scad>;

// --- a2 / b2 / c2 ---

color("green")
  translate([0, 0, 0])
    a2();

color("blue")
  translate([50, 0, 0])
    b2();

color("red")
  translate([100, 0, 0])
    c2();

// --- a3 ---

color("green")
  translate([200, 0, 0])
    a3();

color("red")
  translate([250, 0, 0])
    straightTrack(a3Length, "mortise", "mortise");

color("blue")
  translate([300, 0, 0])
    straightTrack(a3Length, "tenon", "tenon");

// --- a1 ---

color("green")
  translate([400, 0, 0])
    straightTrack(a1Length, "tenon", "mortise");

color("red")
  translate([450, 0, 0])
    straightTrack(a1Length, "mortise", "mortise");

color("blue")
  translate([500, 0, 0])
    straightTrack(a1Length, "tenon", "tenon");

// --- a ---

color("green")
  translate([600, 0, 0])
    straightTrack(aLength, "tenon", "mortise");

color("red")
  translate([650, 0, 0])
    straightTrack(aLength, "mortise", "mortise");

color("blue")
  translate([700, 0, 0])
    straightTrack(aLength, "tenon", "tenon");

// --- d ---

color("green")
  translate([800, 0, 0])
    straightTrack(dLength, "tenon", "mortise");

color("red")
  translate([850, 0, 0])
    straightTrack(dLength, "mortise", "mortise");

color("blue")
  translate([900, 0, 0])
    straightTrack(dLength, "tenon", "tenon");

// --- e1 ---

color("green")
  translate([-50, -300, 0])
    curvedTrack("round", e1Radius, "mortise", "tenon");

color("red")
  translate([25, -300, 0])
    curvedTrack("round", e1Radius, "tenon", "tenon");

color("blue")
  translate([100, -300, 0])
    curvedTrack("round", e1Radius, "mortise", "mortise");

// --- e ---

color("green")
  translate([150, -300, 0])
    curvedTrack("round", eRadius, "mortise", "tenon");

color("red")
  translate([225, -300, 0])
    curvedTrack("round", eRadius, "mortise", "mortise");

color("blue")
  translate([300, -300, 0])
    curvedTrack("round", eRadius, "tenon", "tenon");
