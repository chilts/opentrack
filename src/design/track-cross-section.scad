// ----------------------------------------------------------------------------
//
// DESIGN
//
// This scad file is just a visual representation of all of the variations of:
//
// * style:
//   * "square"
//   * "round"
//   * "triangle"
// * top:
//   * "track"
//   * "road"
//   * "blank"
// * bottom:
//   * "track"
//   * "road"
//   * "blank"
//
// ----------------------------------------------------------------------------

include <../config.scad>;
use <../lib/track.scad>;

offsetCol1 =   0;
offsetCol2 =  60;
offsetCol3 = 120;
offsetCol4 = 180;
offsetCol5 = 240;

// square
translate([offsetCol1, 0, 0])
  track2d("square", "track", "track");
translate([offsetCol1, -20, 0])
  track2d("square", "track", "road");
translate([offsetCol1, -40, 0])
  track2d("square", "track", "blank");
translate([offsetCol1, -60, 0])
  track2d("square", "road", "track");
translate([offsetCol1, -80, 0])
  track2d("square", "road", "road");
translate([offsetCol1, -100, 0])
  track2d("square", "road", "blank");

// round
translate([offsetCol2, 0, 0])
  track2d("round", "track", "track");
translate([offsetCol2, -20, 0])
  track2d("round", "track", "road");
translate([offsetCol2, -40, 0])
  track2d("round", "track", "blank");
translate([offsetCol2, -60, 0])
  track2d("round", "road", "track");
translate([offsetCol2, -80, 0])
  track2d("round", "road", "road");
translate([offsetCol2, -100, 0])
  track2d("round", "road", "blank");

// triangle
translate([offsetCol3, 0, 0])
  track2d("triangle", "track", "track");
translate([offsetCol3, -20, 0])
  track2d("triangle", "track", "road");
translate([offsetCol3, -40, 0])
  track2d("triangle", "track", "blank");
translate([offsetCol3, -60, 0])
  track2d("triangle", "road", "track");
translate([offsetCol3, -80, 0])
  track2d("triangle", "road", "road");
translate([offsetCol3, -100, 0])
  track2d("triangle", "road", "blank");

// corners
translate([offsetCol4, 0, 0])
  corners2d("square");
translate([offsetCol4, -20, 0])
  corners2d("round");
translate([offsetCol4, -40, 0])
  corners2d("triangle");

// rails/roads top/bottom
translate([offsetCol5, 0, 0])
  insets2d("triangle", "track", "track");
translate([offsetCol5, -20, 0])
  insets2d("triangle", "track", "road");
translate([offsetCol5, -40, 0])
  insets2d("triangle", "track", "blank");
translate([offsetCol5, -60, 0])
  insets2d("triangle", "road", "track");
translate([offsetCol5, -80, 0])
  insets2d("triangle", "road", "road");
translate([offsetCol5, -100, 0])
  insets2d("triangle", "road", "blank");

// ----------------------------------------------------------------------------
