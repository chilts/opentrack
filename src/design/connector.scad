include <../config.scad>;
use <../lib/connector.scad>;
use <../lib/track.scad>;

// check the tenon neck is the correct width, length, and height
translate([-10, 3, 0])
  cube([tenonNeckWidth, tenonNeckLength, trackHeight], center=true);

translate([0, 0, 0])
  connector("tenon");
translate([20, 0, 0])
  connector("mortise");

translate([31, -3.25, 0])
  cube([mortiseNeckWidth, mortiseNeckLength, trackHeight], center=true);
