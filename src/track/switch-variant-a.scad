//+ {
//+   "name": "switch-variant-a",
//+   "title": "Switch (Variant A)",
//+   "brio": "F",
//+   "type": "part",
//+   "specs": {
//+     "type": "Switch",
//+     "length": "144",
//+     "top": "Track",
//+     "bottom": "Blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-04-29T10:51:04.119Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/connector.scad>;
use <../lib/track.scad>;
use <./switch-base.scad>;

// the track splits left and right the same amount
offset = doubleWidthOffsetFromCentre;
length = aLength;
top = "track";
bottom = "blank";

module SwitchingTrackVariantA() {
  // same length as 'A' track
  difference() {
    union() {
      SwitchBase(length, defaultStyle, top, bottom);

      // add the tenon connectors
      translate([-offset, length + tenonTotalLength/2, 0])
        mirror([0, 1, 0])
          connector("tenon", top, bottom);

      translate([offset, length + tenonTotalLength/2, 0])
        mirror([0, 1, 0])
          connector("tenon", top, bottom);
    }

    // remove the mortise connector
    translate([0, mortiseTotalLength/2, 0])
      connector("mortise");
  }
}

SwitchingTrackVariantA();
