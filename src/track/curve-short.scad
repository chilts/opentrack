//+ {
//+   "name": "curve-short",
//+   "title": "Short Curve",
//+   "brio": "E1",
//+   "type": "part",
//+   "specs": {
//+     "type": "Curve",
//+     "angle": "45",
//+     "radius": "90",
//+     "top": "Track",
//+     "bottom": "Track",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-03-30T09:02:21.317Z"
//+ }
//+
//+ ---
//+
//+ The curves have track on both sides so they can be conveniently flipped where
//+ needed.
//+

include <../config.scad>;
use <../lib/curved-track.scad>;

module CurveShort() {
  curvedTrack(e1Radius, bottom = "track", angle = defaultAngle);
}

// move the curve back to the origin
translate([-e1Radius, 0, 0])
  rotate([0, 0, -defaultAngle/2])
    CurveShort();
