//+ {
//+   "name": "straight-short-variant-a",
//+   "title": "Short Straight (108mm) (Variant A)",
//+   "brio": "A1",
//+   "type": "part",
//+   "specs": {
//+     "type": "Straight",
//+     "length": "108",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-04-17T09:21:30.725Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

module StraightShortVariantA() {
  straightTrack(a1Length);
}

StraightShortVariantA();
