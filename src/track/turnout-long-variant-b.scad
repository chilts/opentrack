//+ {
//+   "name": "turnout-long-variant-b",
//+   "title": "Long Turnout (Variant B)",
//+   "type": "part",
//+   "specs": {
//+     "type": "Switch",
//+     "length": "216",
//+     "radius": "90",
//+     "top": "Track",
//+     "bottom": "Track",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-04-16T10:33:48.832Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/track.scad>;
use <../lib/straight-base.scad>;
use <../lib/curved-base.scad>;

// "M"
module TurnoutLongVariantB() {
  difference() {
    union() {
      translate([0, aLength/2, 0])
        straightBase(aLength, connector1 = "mortise", connector2 = "tenon");
      translate([0 - eRadius, 0, 0])
        curvedBase(eRadius, connector1 = "mortise", connector2 = "tenon");
    }

    // remove straight track
    translate([0, aLength/2, 0])
      makeStraight(aLength)
        insets2d(defaultStyle, "track", "track");

    // remove curved track
    translate([0 - eRadius, 0, 0])
      makeCurved(eRadius)
        insets2d(defaultStyle, "track", "track");
  }
}

TurnoutLongVariantB();
