//+ {
//+   "name": "switch-variant-b",
//+   "title": "Switch (Variant B)",
//+   "brio": "G",
//+   "type": "part",
//+   "specs": {
//+     "type": "Switch",
//+     "length": "144",
//+     "top": "Track",
//+     "bottom": "Blank",
//+     "connector1": "tenon",
//+     "connector2": "mortise"
//+   },
//+   "published": "2020-04-29T10:51:04.119Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/connector.scad>;
use <../lib/track.scad>;
use <./switch-base.scad>;

// the track splits left and right the same amount
offset = doubleWidthOffsetFromCentre;
length = aLength;
style = defaultStyle;
top = "track";
bottom = "blank";

module SwitchingTrackVariantB() {
  // same length as 'A' track
  difference() {
    union() {
      SwitchBase(length, defaultStyle, top, bottom);

      // add the tenon connector
      translate([0, 0 - tenonTotalLength/2, 0])
        connector("tenon");
    }

    // remove the mortise connectors
    translate([-offset, length - mortiseTotalLength/2, 0])
      mirror([0, 1, 0])
        connector("mortise");
    translate([offset, length - mortiseTotalLength/2, 0])
      mirror([0, 1, 0])
        connector("mortise");
  }
}

SwitchingTrackVariantB();
