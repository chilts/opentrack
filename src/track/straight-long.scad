//+ {
//+   "name": "straight-long",
//+   "title": "Long Straight (216mm)",
//+   "brio": "D",
//+   "type": "part",
//+   "specs": {
//+     "type": "Straight",
//+     "length": "216",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-03-27T22:19:31.049Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

module StraightLong() {
  straightTrack(dLength);
}

StraightLong();
