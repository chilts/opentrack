//+ {
//+   "name": "switch-curve-long-variant-b",
//+   "title": "Switch Curve Long (Variant B)",
//+   "bases": [
//+     "curve-long"
//+   ],
//+   "type": "part",
//+   "specs": {
//+     "type": "Switch",
//+     "angle": "45",
//+     "radius": "170",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-04-26T11:27:52.739Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/track.scad>;
use <../lib/curved-base.scad>;

// No Brio number. Long version of "P".
module SwitchCurveLongVariantB() {
  difference() {
    union() {
      translate([0 - eRadius, 0, 0])
        curvedBase(eRadius, connector1 = "mortise", connector2 = "tenon");
      mirror([1, 0, 0])
        translate([0 - eRadius, 0, 0])
          curvedBase(eRadius, connector1 = "mortise", connector2 = "tenon");
    }

    // remove curved track (left)
    translate([0 - eRadius, 0, 0])
      makeCurved(eRadius)
        insets2d(defaultStyle, "track", "blank");

    // remove curved track (right)
    mirror([1, 0, 0])
      translate([0 - eRadius, 0, 0])
        makeCurved(eRadius)
          insets2d(defaultStyle, "track", "blank");
  }
}

SwitchCurveLongVariantB();
