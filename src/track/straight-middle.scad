//+ {
//+   "name": "straight-middle",
//+   "title": "Middle Straight (72mm)",
//+   "brio": "A3",
//+   "type": "part",
//+   "specs": {
//+     "type": "Straight",
//+     "length": "72",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-03-27T22:19:31.049Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

module StraightMiddle() {
  straightTrack(a3Length);
}

StraightMiddle();
