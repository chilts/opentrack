//+ {
//+   "name": "switch-curve-long-variant-a",
//+   "title": "Switch Curve Long (Variant A)",
//+   "bases": [
//+     "curve-long"
//+   ],
//+   "type": "part",
//+   "specs": {
//+     "type": "Switch",
//+     "angle": "45",
//+     "radius": "170",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-04-26T11:27:52.739Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/track.scad>;
use <../lib/curved-base.scad>;

// No Brio number. Long version of "O".
module SwitchCurveLongVariantA() {
  difference() {
    union() {
      translate([0 - eRadius, 0, 0])
        curvedBase(eRadius);
      mirror([1, 0, 0])
        translate([0 - eRadius, 0, 0])
          curvedBase(eRadius);
    }

    // remove curved track (left)
    translate([0 - eRadius, 0, 0])
      makeCurved(eRadius)
        insets2d(defaultStyle, "track", "blank");

    // remove curved track (right)
    mirror([1, 0, 0])
      translate([0 - eRadius, 0, 0])
        makeCurved(eRadius)
          insets2d(defaultStyle, "track", "blank");
  }
}

SwitchCurveLongVariantA();
