//+ {
//+   "name": "straight-mini-variant-a",
//+   "title": "Mini Straight (54mm) (Variant A)",
//+   "brio": "A2",
//+   "type": "part",
//+   "specs": {
//+     "type": "Straight",
//+     "length": "54",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-03-27T22:19:31.049Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

// "A2"
module StraightMiniVariantA() {
  straightTrack(a2Length);
}

StraightMiniVariantA();
