include <../config.scad>;
use <../lib/track.scad>;
use <../lib/straight-base.scad>;
use <../lib/straight-track.scad>;
use <../lib/curved-base.scad>;
use <../lib/curved-track.scad>;

length = dLength;

// Because we're 3D printing, we can print a track on both sides instead of only
// having it on one side. :) Aw yeah.

// connector1 - closest on straight
// connector2 - furthest on straight
// connector3 - apex of both curves
module SwitchTBase(connector1, connector2, connector3) {
  // same length as 'D' track
  difference() {
    union() {
      straightTrack(
        length - 2 * railDistFromEdge,
        connector1 = connector2,
        connector2 = connector1,
        top = "blank",
        bottom = "blank"
      );

      // right curve (as viewed from straight)
      translate([length/2 - railDistFromEdge, -length/2 + railDistFromEdge, 0]) {
        mirror([1, 0, 0]) {
          curvedTrack(
            tRadius,
            connector1 = connector1,
            connector2 = connector3,
            top = "blank",
            bottom = "blank",
            angle = tAngle
          );
        }
      }

      // left curve (as viewed from straight)
      translate([length/2 - railDistFromEdge, length/2 - railDistFromEdge, 0]) {
        mirror([1, 1, 0]) {
          curvedTrack(
            tRadius,
            connector1 = connector2,
            connector2 = connector3,
            top = "blank",
            bottom = "blank",
            angle = tAngle
          );
        }
      }
    }

    // remove straight track
    makeStraight(length)
      insets2d(defaultStyle, "track", "track");

    // remove curved track (left)
    translate([length/2 - railDistFromEdge, -length/2 + railDistFromEdge, 0])
      mirror([1, 0, 0])
        makeCurved(tRadius, angle = tAngle)
          insets2d(defaultStyle, "track", "track");

    // remove curved track (right)
    translate([length/2 - railDistFromEdge, length/2 - railDistFromEdge, 0])
      mirror([1, 1, 0])
        makeCurved(tRadius, angle = tAngle)
          insets2d(defaultStyle, "track", "track");
  }
}
