//+ {
//+   "name": "turnout-long-x4-variant-b",
//+   "title": "Long Turnout x4 (Variant B)",
//+   "type": "part",
//+   "specs": {
//+     "type": "Switch",
//+     "length": "216",
//+     "radius": "90",
//+     "top": "Track",
//+     "bottom": "Track",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-04-16T10:33:48.832Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/track.scad>;
use <./turnout-long-x4-base.scad>;

module TurnoutLongVariantB() {
  TurnoutLongX4Base("mortise", "tenon");
}

TurnoutLongVariantB();
