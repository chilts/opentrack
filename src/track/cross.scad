//+ {
//+   "name": "cross",
//+   "title": "Cross",
//+   "brio": "H1",
//+   "type": "part",
//+   "specs": {
//+     "type": "Junction",
//+     "length": "116",
//+     "top": "Track",
//+     "bottom": "Blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-04-02T10:57:35.852Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/track.scad>;
use <../lib/straight-base.scad>;

// ToDo: this cross track has 4 curves in each corner which makes
// it look a bit nicer. I don't think it gives it much extra strength
// but might be worth adding anyway.

// H1 doesn't need an "Alt" (where the mortise and tenon are reversed on one
// track) since you can just rotate it by 90deg for the same effect.

// From : https://woodenrailway.info/track/brio-track-guide
//
// H1 is a strange piece with a very specific use. It is a cross track but the
// opposite ends have the same connector (male-to-male, and
// female-to-female). It's straights are 116mm long, which does not match any BRIO
// standard pieces.
//
// It's designed to form a cross from two or more curved switch tracks. You can
// fit H1 inside the space of an L and M joined at the curved end, so that the
// straight tracks cross while the curved tracks form a quarter circle. It can
// also fit inside the gap formed by joining two I and two J switches together at
// their curved ends, to make a very large "star" switch.

length = 116;

module Cross(length) {
  difference() {
    union() {
      translate([0, length/2, 0])
        straightBase(length);
      rotate([0, 0, 90])
        translate([length/2, 0, 0])
          straightBase(length);
    }

    // remove straight track
    translate([0, length/2, 0])
      makeStraight(length)
        insets2d(defaultStyle, "track", "blank");
    rotate([0, 0, 90])
      translate([length/2, 0, 0])
        makeStraight(length)
          insets2d(defaultStyle, "track", "blank");
  }
}

Cross(length);
