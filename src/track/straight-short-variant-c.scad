//+ {
//+   "name": "straight-short-variant-c",
//+   "title": "Short Straight (108mm) (Variant c)",
//+   "brio": "C1",
//+   "type": "part",
//+   "specs": {
//+     "type": "Straight",
//+     "length": "108",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "mortise"
//+   },
//+   "published": "2020-04-17T09:21:30.725Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

module StraightShortVariantC() {
  straightTrack(a1Length, connector1 = "mortise");
}

StraightShortVariantC();
