include <../config.scad>;
use <../lib/connector.scad>;
use <../lib/track.scad>;

// the track splits left and right the same amount
offset = doubleWidthOffsetFromCentre;
length = aLength;

module SwitchBase(length, style, top, bottom) {
  // same length as 'A' track
  difference() {
    hull() {
      // first base
      difference() {
        makeShear(length, offset)
          translate([0, length/2, 0])
            makeStraight(length)
              track2dBase(style);
        makeShear(length, offset)
          translate([0, length/2, 0])
            makeStraight(length)
              corners2d(style);
      }

      // second base
      difference() {
        makeShear(length, -offset)
          translate([0, length/2, 0])
            makeStraight(length)
              track2dBase(style);
        makeShear(length, -offset)
          translate([0, length/2, 0])
            makeStraight(length)
              corners2d(style);
      }
    }

    // take off the rails
    makeShear(length, offset)
      translate([0, length/2, 0])
        makeStraight(length)
          insets2d(style, top, bottom);
    makeShear(length, -offset)
      translate([0, length/2, 0])
        makeStraight(length)
          insets2d(style, top, bottom);
  }
}
