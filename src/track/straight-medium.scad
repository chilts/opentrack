//+ {
//+   "name": "straight-medium",
//+   "title": "Medium Straight (144mm)",
//+   "brio": "A",
//+   "type": "part",
//+   "specs": {
//+     "type": "Straight",
//+     "length": "144",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-03-27T22:19:31.049Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

module StraightMedium() {
  straightTrack(aLength);
}

StraightMedium();
