//+ {
//+   "name": "switch-t-variant-a",
//+   "title": "Switch T (Variant A)",
//+   "bases": [
//+     "curve-long"
//+   ],
//+   "type": "part",
//+   "specs": {
//+     "type": "Switch",
//+     "length": "216",
//+     "angle": "45",
//+     "radius": "170",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon",
//+     "connector3": "mortise"
//+   },
//+   "published": "2020-04-26T11:27:52.739Z"
//+ }
//+
//+ ---
//+

use <switch-t-base.scad>;

// ToDo: fix this so the curves match up, then just use the next straight up to
// connect them (centered).

module SwitchTVariantA() {
  SwitchTBase(connector1 = "mortise", connector2 = "tenon", connector3 = "mortise");
}

SwitchTVariantA();
