//+ {
//+   "name": "switch-curve-short-variant-a",
//+   "title": "Switch Curve Short (Variant A)",
//+   "brio": "O",
//+   "bases": [
//+     "curve-short"
//+   ],
//+   "type": "part",
//+   "specs": {
//+     "type": "Switch",
//+     "angle": "45",
//+     "radius": "90",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "tenon",
//+     "connector2": "mortise"
//+   },
//+   "published": "2020-04-26T11:27:52.739Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/track.scad>;
use <../lib/curved-base.scad>;

// "P"
module SwitchCurveShortVariantB() {
  difference() {
    union() {
      translate([0 - e1Radius, 0, 0])
        curvedBase(e1Radius, connector1 = "mortise", connector2 = "tenon");
      mirror([1, 0, 0])
        translate([0 - e1Radius, 0, 0])
          curvedBase(e1Radius, connector1 = "mortise", connector2 = "tenon");
    }

    // remove curved track (left)
    translate([0 - e1Radius, 0, 0])
      makeCurved(e1Radius)
        insets2d(defaultStyle, "track", "blank");

    // remove curved track (right)
    mirror([1, 0, 0])
      translate([0 - e1Radius, 0, 0])
        makeCurved(e1Radius)
          insets2d(defaultStyle, "track", "blank");
  }
}

SwitchCurveShortVariantB();
