//+ {
//+   "name": "switch-curve-short-variant-b",
//+   "title": "Switch Curve Short (Variant B)",
//+   "brio": "O",
//+   "bases": [
//+     "curve-short"
//+   ],
//+   "type": "part",
//+   "specs": {
//+     "type": "Switch",
//+     "angle": "45",
//+     "radius": "90",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-04-26T11:27:52.739Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/track.scad>;
use <../lib/curved-base.scad>;

// "O"
module SwitchCurveShortVariantA() {
  difference() {
    union() {
      translate([0 - e1Radius, 0, 0])
        curvedBase(e1Radius);
      mirror([1, 0, 0])
        translate([0 - e1Radius, 0, 0])
          curvedBase(e1Radius);
    }

    // remove curved track (left)
    translate([0 - e1Radius, 0, 0])
      makeCurved(e1Radius)
        insets2d(defaultStyle, "track", "blank");

    // remove curved track (right)
    mirror([1, 0, 0])
      translate([0 - e1Radius, 0, 0])
        makeCurved(e1Radius)
          insets2d(defaultStyle, "track", "blank");
  }
}

SwitchCurveShortVariantA();
