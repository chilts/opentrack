//+ {
//+   "name": "turnout-long-x4-variant-a",
//+   "title": "Long Turnout x4 (Variant A)",
//+   "type": "part",
//+   "specs": {
//+     "type": "Switch",
//+     "length": "216",
//+     "radius": "90",
//+     "top": "Track",
//+     "bottom": "Track",
//+     "connector1": "tenon",
//+     "connector2": "mortise"
//+   },
//+   "published": "2020-04-16T10:33:48.832Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/track.scad>;
use <./turnout-long-x4-base.scad>;

module TurnoutLongVariantA() {
  TurnoutLongX4Base("tenon", "mortise");
}

TurnoutLongVariantA();
