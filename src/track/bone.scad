//+ {
//+   "name": "bone",
//+   "title": "Bone",
//+   "type": "part",
//+   "specs": {
//+     "type": "Connector",
//+     "connector1": "tenon",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-04-09T11:57:42.307Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/connector.scad>;

// make both parts overlap so OpenSCAD knows they are the same object
offset = 0.1;

union() {
  translate([0, 0 - tenonTotalLength / 2 - offset, 0])
    connector("tenon");
  translate([0, 0 + tenonTotalLength / 2 + offset, 0])
    mirror([0, 1, 0])
      connector("tenon");
}
