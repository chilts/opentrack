include <../config.scad>;
use <../lib/track.scad>;
use <../lib/straight-base.scad>;
use <../lib/curved-base.scad>;

// ToDo: it looks like the outside two are just long curves, they look closer
// to a short curve with a straight bit on the end! The straight is still a
// straight. I dunno about the other two.

length = 180;
halfAngle = defaultAngle / 2;
halfAngleRadius = 2 * eRadius;

angle1 = 49;
angle2 = angle1 / 2;
radius1 = eRadius;
radius2 = 415;

// "Q" variants "A" and "B"
module TurnoutLongX4Base(connector1, connector2) {
  // echo("eRadius:", eRadius);
  // echo("halfAngleRadius:", halfAngleRadius);
  // echo("angle1=", angle1);
  // echo("angle2=", angle2);
  // echo("radius1=", radius1);
  // echo("radius2=", radius2);

  difference() {
    union() {
      translate([0, length/2, 0])
        straightBase(length, connector1 = connector1, connector2 = connector2);

      // two tracks at the regular angle of 45 degrees
      translate([0 - radius1, 0, 0])
        curvedBase(radius1, angle = angle1, connector1 = connector1, connector2 = connector2);
      mirror([1, 0, 0])
        translate([0 - radius1, 0, 0])
          curvedBase(radius1, angle = angle1, connector1 = connector1, connector2 = connector2);

      // the two tracks at 22.5 degrees
      translate([0 - radius2, 0, 0])
        curvedBase(radius2, angle = angle2, connector1 = connector1, connector2 = connector2);

      mirror([1, 0, 0])
        translate([0 - radius2, 0, 0])
          curvedBase(radius2, angle = angle2, connector1 = connector1, connector2 = connector2);
    }

    // remove straight track
    translate([0, length/2, 0])
      makeStraight(length)
        insets2d(defaultStyle, "track", "blank");

    // remove curved track (left)
    translate([0 - radius1, 0, 0])
      makeCurved(radius1, angle = angle1)
        insets2d(defaultStyle, "track", "blank");

    // remove curved track (right)
    mirror([1, 0, 0])
      translate([0 - radius1, 0, 0])
        makeCurved(radius1, angle = angle1)
          insets2d(defaultStyle, "track", "blank");

    // remove curved track (half-left)
    translate([0 - radius2, 0, 0])
      makeCurved(radius2, angle = angle2)
        insets2d(defaultStyle, "track", "blank");

    // remove curved track (half-right)
    mirror([1, 0, 0])
      translate([0 - radius2, 0, 0])
        makeCurved(radius2, angle = angle2)
          insets2d(defaultStyle, "track", "blank");
  }
}
