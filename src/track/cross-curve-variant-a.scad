//+ {
//+   "name": "cross-curve-variant-a",
//+   "title": "Cross Curve (Variant A)",
//+   "type": "part",
//+   "specs": {
//+     "type": "Junction",
//+     "radius": "90",
//+     "angle": "90",
//+     "top": "Track",
//+     "bottom": "Blank",
//+     "connector1": "mortise",
//+     "connector1": "tenon"
//+   },
//+   "published": "2020-04-29T11:14:14.665Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <./cross-curve-base.scad>;

// 49.95 stops each track encroaching inside the other's mortise, and also
// stops the ends of the track stick out too far (in the middle). Try
// `defaultAngle` (45) and you'll see both artifacts.

// CrossCurveBase(eRadius, 49.95, "tenon", "mortise");
// CrossCurveBase(eRadius, 60, "tenon", "mortise");
CrossCurveBase(e1Radius, 90, "tenon", "mortise");
