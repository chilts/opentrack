//+ {
//+   "name": "straight-short-variant-b",
//+   "title": "Short Straight (108mm) (Variant B)",
//+   "brio": "B1",
//+   "type": "part",
//+   "specs": {
//+     "type": "Straight",
//+     "length": "108",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "tenon",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-04-17T09:21:30.725Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

module StraightShortVariantB() {
  straightTrack(a1Length, connector2 = "tenon");
}

StraightShortVariantB();
