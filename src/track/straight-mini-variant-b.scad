//+ {
//+   "name": "straight-mini-variant-b",
//+   "title": "Mini Straight (54mm) (Variant B)",
//+   "brio": "B2",
//+   "type": "part",
//+   "specs": {
//+     "type": "Straight",
//+     "length": "54",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "tenon",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-03-29T00:47:17.992Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

// "B2"
module StraightMiniVariantB() {
  straightTrack(a2Length, connector2 = "tenon");
}

StraightMiniVariantB();
