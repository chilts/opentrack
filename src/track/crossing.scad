//+ {
//+   "name": "crossing",
//+   "title": "Crossing",
//+   "brio": "H",
//+   "type": "part",
//+   "specs": {
//+     "type": "Junction",
//+     "length": "108",
//+     "top": "Track",
//+     "bottom": "Blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-04-02T10:57:35.852Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/track.scad>;
use <../lib/straight-base.scad>;

module Crossing() {
  difference() {
    union() {
      translate([0, aLength/2, 0])
        straightBase(aLength);
      rotate([0, 0, 90])
        translate([aLength/2, 0, 0])
          straightBase(aLength);
    }

    // remove straight track
    translate([0, aLength/2, 0])
      makeStraight(aLength)
        insets2d(defaultStyle, "track", "blank");
    rotate([0, 0, 90])
      translate([aLength/2, 0, 0])
        makeStraight(aLength)
          insets2d(defaultStyle, "track", "blank");
  }
}

Crossing();
