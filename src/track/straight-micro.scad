//+ {
//+   "name": "straight-micro",
//+   "title": "Micro Straight (27mm)",
//+   "type": "part",
//+   "specs": {
//+     "type": "Straight",
//+     "length": "27",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-07-25T22:02:06.330Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

module StraightMicro() {
  straightTrack(microLength);
}

StraightMicro();
