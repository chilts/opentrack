//+ {
//+   "name": "straight-mini-variant-c",
//+   "title": "Mini Straight (54mm) (Variant C)",
//+   "brio": "C2",
//+   "type": "part",
//+   "specs": {
//+     "type": "Straight",
//+     "length": "54",
//+     "top": "track",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "mortise"
//+   },
//+   "published": "2020-03-27T22:19:31.049Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

// "C2"
module StraightMiniVariantC() {
  straightTrack(a2Length, connector1 = "mortise");
}

StraightMiniVariantC();
