//+ {
//+   "name": "H2-Alt",
//+   "title": "H2 Criss-Cross (Alt)",
//+   "type": "part",
//+   "specs": {
//+     "type": "Junction",
//+     "length": "144",
//+     "top": "Track",
//+     "bottom": "Track",
//+     "connector1": "tenon",
//+     "connector2": "mortise"
//+   },
//+   "published": "2020-04-02T10:57:35.852Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/track.scad>;
use <../lib/straight-base.scad>;

// "H2" Variant
module CrissCrossVariantB(length) {
  difference() {
    union() {
      straightBase(length, connector1 = "mortise", connector2 = "tenon");
      rotate([0, 0, 45])
        straightBase(length);
    }

    // remove straight track
    makeStraight(length)
      insets2d(defaultStyle, "track", "blank");
    rotate([0, 0, 45])
      makeStraight(length)
        insets2d(defaultStyle, "track", "blank");
  }
}

CrissCrossVariantB(aLength);
