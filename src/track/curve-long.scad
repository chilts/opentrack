//+ {
//+   "name": "curve-long",
//+   "title": "Long Curve",
//+   "brio": "E",
//+   "type": "part",
//+   "specs": {
//+     "type": "Curve",
//+     "angle": "45",
//+     "radius": "170",
//+     "top": "Track",
//+     "bottom": "Track",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-03-30T09:02:21.317Z"
//+ }
//+
//+ ---
//+
//+ The curves have track on both sides so they can be conveniently flipped where
//+ needed.
//+

include <../config.scad>;
use <../lib/curved-track.scad>;

module CurveLarge() {
  curvedTrack(eRadius, bottom = "track", angle = defaultAngle);
}

// move the curve back to the origin
translate([-eRadius, 0, 0])
  rotate([0, 0, -defaultAngle/2])
    CurveLarge();
