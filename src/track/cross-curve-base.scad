include <../config.scad>;
use <../lib/track.scad>;
use <../lib/curved-base.scad>;

module translateA(radius, angle) {
  translate([-radius, 0, 0])
    rotate([0, 0, -angle/2])
      children();
}

module translateB(radius, angle) {
  translate([radius, 0, 0])
    rotate([0, 0, 180])
      rotate([0, 0, -angle/2])
        children();
}

// "H3"
module CrossCurveBase(radius, angle, connector1, connector2) {
  difference() {
    union() {
      translateA(radius, angle)
        curvedBase(radius, angle = angle, bottom = "track");
      translateB(radius, angle)
        curvedBase(radius, angle = angle, connector1 = connector2, connector2 = connector1, bottom = "track");
    }

    // remove curved track
    translateA(radius, angle)
      makeCurved(radius, angle)
        insets2d(defaultStyle, "track", "track");
    translateB(radius, angle)
      makeCurved(radius, angle)
        insets2d(defaultStyle, "track", "track");
  }
}
