include <../config.scad>;
use <../lib/straight-track.scad>;

module b2RoadOnly() {
  straightTrack(a2Length, connector2 = "tenon", top = "road", bottom = "blank");
}

b2RoadOnly();
