include <../config.scad>;
use <../lib/straight-track.scad>;

module b2Road() {
  straightTrack(a2Length, connector2 = "tenon", top = "road", bottom = "track");
}

b2Road();
