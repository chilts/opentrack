//+ {
//+   "name": "a1-road",
//+   "title": "A1 Road",
//+   "brio": "A1 Road",
//+   "type": "part",
//+   "specs": {
//+     "type": "Junction",
//+     "length": "144",
//+     "top": "Road",
//+     "bottom": "Track",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-04-26T11:14:34.060Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

module a1Road() {
  straightTrack(a1Length, top = "road", bottom = "track");
}

a1Road();
