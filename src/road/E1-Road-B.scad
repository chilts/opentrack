include <../config.scad>;
use <../lib/curved-track.scad>;

module e1RoadB() {
  mirror([0, 1, 0])
    curvedTrack(e1Radius, top = "road", bottom = "track");
}

e1RoadB();
