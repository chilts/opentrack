include <../config.scad>;
use <../lib/straight-track.scad>;

module a2Road() {
  straightTrack(a2Length, top = "road", bottom = "track");
}

a2Road();
