include <../config.scad>;
use <../lib/straight-track.scad>;

module DRoad() {
  straightTrack(dLength, top = "road", bottom = "track");
}

DRoad();
