include <../config.scad>;
use <../lib/curved-track.scad>;

module e1RoadA() {
  curvedTrack(e1Radius, top = "road", bottom = "track");
}

e1RoadA();
