include <../config.scad>;
use <../lib/straight-track.scad>;

module c2RoadOnly() {
  straightTrack(a2Length, connector1 = "mortise", top = "road", bottom = "blank");
}

c2RoadOnly();
