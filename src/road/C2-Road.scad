include <../config.scad>;
use <../lib/straight-track.scad>;

module c2Road() {
  straightTrack(a2Length, connector1 = "mortise", top = "road", bottom = "track");
}

c2Road();
