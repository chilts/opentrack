//+ {
//+   "name": "a2-road-only",
//+   "title": "A2 Road",
//+   "brio": "A2 Road",
//+   "type": "part",
//+   "specs": {
//+     "type": "Straight",
//+     "length": "144",
//+     "top": "road",
//+     "bottom": "blank",
//+     "connector1": "mortise",
//+     "connector2": "tenon"
//+   },
//+   "published": "2020-04-26T11:15:36.259Z"
//+ }
//+
//+ ---
//+

include <../config.scad>;
use <../lib/straight-track.scad>;

module a2RoadOnly() {
  straightTrack(a2Length, top = "road", bottom = "blank");
}

a2RoadOnly();
