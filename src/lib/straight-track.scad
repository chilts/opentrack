include <../config.scad>;
use <track.scad>;
use <straight-base.scad>;

// draw a regular piece of track
module straightTrack(
  length,
  style = defaultStyle,
  top = "track",
  bottom = "blank",
  connector1 = "tenon",
  connector2 = "mortise"
) {
  union() {
    difference() {
      straightBase(
        length,
        style,
        top,
        bottom,
        connector1,
        connector2
      );
      makeStraight(length + epsilon)
        corners2d(style);
      makeStraight(length + epsilon)
        insets2d(style, top, bottom);
    }

    // add the road markings on
    makeRoadMarkings(length, style, top, bottom, connector1, connector2);
  }
}

// draw a regular piece of track
module straightDouble(
  length,
  style = defaultStyle,
  top = "track",
  bottom = "blank",
  connector1 = "tenon",
  connector2 = "mortise"
) {
  union() {
    difference() {
      straightDoubleBase(
        length,
        style,
        top,
        bottom,
        connector1,
        connector2
      );
      makeStraight(length + epsilon)
        corners2d(style, width = doubleWidth);
      translate([-doubleWidthOffsetFromCentre, 0, 0])
        makeStraight(length + epsilon)
          insets2d(style, top, bottom);
      translate([doubleWidthOffsetFromCentre, 0, 0])
        makeStraight(length + epsilon)
          insets2d(style, top, bottom);
    }

    // add the road markings on
    translate([-doubleWidthOffsetFromCentre, 0, 0])
      makeRoadMarkings(length, style, top, bottom, connector1, connector2);
    translate([doubleWidthOffsetFromCentre, 0, 0])
      makeRoadMarkings(length, style, top, bottom, connector1, connector2);
  }
}
