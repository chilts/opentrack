include <../config.scad>;

module neck(
  type,
  width,
  length,
  height = trackHeight
) {
  if (type == "tenon") {
    difference() {
      minkowski() {
        cube([width - 2, length, height - 2], center=true);
        sphere(d=2);
      }
      // take the end bevel off
      translate([0, length / 2 + 0.5, 0])
        cube([width, 1, height], center=true);
    }
  }

  if (type == "mortise") {
    cube([width, length, height + epsilon], center=true);
  }
}

module connector(type, top, bottom) {
  thisDiameter = type == "tenon" ? tenonDiameter : mortiseDiameter;
  thisNeckWidth = type == "tenon" ? tenonNeckWidth : mortiseNeckWidth;
  thisNeckLength = type == "tenon" ? tenonNeckLength : mortiseNeckLength;
  thisCylinderTranslate = type == "tenon" ? 0 - thisNeckLength / 2 : thisNeckLength / 2;

  if (type == "tenon") {
    // tenon connectors height and zOffset differ slightly if a road is involved
    tenonHeight = trackHeight - (top == "road" ? roadDepth : 0) - (bottom == "road" ? roadDepth : 0);
    heightOffset = 0 - (top == "road" ? roadDepth/2 : 0 ) + (bottom == "road" ? roadDepth/2 : 0 );

    // length = -11 to 5.5 = 16.5 (so translate by a bit, to get )
    translate([0, 3, 0]) {
      // neck
      translate([0, 0, heightOffset])
        neck(type, thisNeckWidth, thisNeckLength, tenonHeight);

      // end cylinder
      translate([0, 0 - thisNeckLength/2, heightOffset]) {
        minkowski() {
          cylinder(h = tenonHeight - 2, d = thisDiameter - 2, center = true);
          sphere(d = 2);
        }
      }
    }

  }
  else {
    // mortise
    // length = -12.75 to 5.75 = 17.5 (so translate by a bit, to get -8.75 to 8.75)
    translate([0, -3.25, 0]) {
      neck("mortise", thisNeckWidth, thisNeckLength, trackHeight);
    translate([0, thisCylinderTranslate, 0])
      cylinder(h=trackHeight + epsilon, d=thisDiameter, center=true);
    }
  }
}
