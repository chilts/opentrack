include <../config.scad>;

module quarterBevelTriangle(len, leftOrRight, topOrBottom) {
  if (leftOrRight == "left" && topOrBottom == "top") {
    polygon([ [-len/2, len/2], [0, len/2], [-len/2, 0] ]);
  }

  if (leftOrRight == "right" && topOrBottom == "top") {
    polygon([ [len/2, len/2], [0, len/2], [len/2, 0] ]);
  }

  if (leftOrRight == "left" && topOrBottom == "bottom") {
    polygon([ [-len/2, -len/2], [0, -len/2], [-len/2, 0] ]);
  }

  if (leftOrRight == "right" && topOrBottom == "bottom") {
    polygon([ [len/2, -len/2], [0, -len/2], [len/2, 0] ]);
  }
}

module quarterBevelRound(diam, leftOrRight, topOrBottom) {
  difference() {
    square([diam, diam], center=true);
    circle(d=diam, $fn=36);

    // X axis
    if (leftOrRight == "left") {
      translate([diam/4, 0])
        square([diam/2, diam], center=true);
    }
    else {
      // right
      translate([0-diam/4, 0])
        square([diam/2, diam], center=true);
    }

    // Y axis
    if (topOrBottom == "bottom") {
      translate([0, diam/4])
        square([diam, diam/2], center=true);
    }
    else {
      // top
      translate([0, 0-diam/4])
        square([diam, diam/2], center=true);
    }
  }
}

module railChannel(style) {
  union() {
    square([railWidth, railDepth + epsilon], center=true);
    if (style == "square") {
      // no need to do anything else
    }

    if (style == "round") {
      translate([0 - railWidth/2 - roundBevel/2, railDepth/2 - roundBevel/2])
        quarterBevelRound(roundBevel + epsilon, "right", "top");
      translate([0 + railWidth/2 + roundBevel/2, railDepth/2 - roundBevel/2])
        quarterBevelRound(roundBevel + epsilon, "left", "top");
    }

    if (style == "triangle") {
      translate([0 - railWidth/2 - triangleBevel/2, railDepth/2 - triangleBevel/2])
        quarterBevelTriangle(triangleBevel, "right", "top");
      translate([0 + railWidth/2 + triangleBevel/2, railDepth/2 - triangleBevel/2])
        quarterBevelTriangle(triangleBevel, "left", "top");
    }
  }
}

module roadChannel(style) {
  union() {
    square([roadWidth, roadDepth + epsilon], center=true);
    if (style == "square") {
      // no need to do anything else
    }
    if (style == "round") {
      translate([0 - roadWidth/2 - roundBevel/2, roadDepth/2 - roundBevel/2])
        quarterBevelRound(roundBevel + epsilon, "right", "top");
      translate([0 + roadWidth/2 + roundBevel/2, roadDepth/2 - roundBevel/2])
        quarterBevelRound(roundBevel + epsilon, "left", "top");
    }

    if (style == "triangle") {
      translate([0 - roadWidth/2 - triangleBevel/2, roadDepth/2 - triangleBevel/2])
        quarterBevelTriangle(triangleBevel, "right", "top");
      translate([0 + roadWidth/2 + triangleBevel/2, roadDepth/2 - triangleBevel/2])
        quarterBevelTriangle(triangleBevel, "left", "top");
    }
  }
}

module topTrack(style) {
  // left
  translate([0 - railDiff/2, trackHeight/2 - railDepth/2])
    railChannel(style);
  // right
  translate([0 + railDiff/2, trackHeight/2 - railDepth/2])
    railChannel(style);
}

module bottomTrack(style) {
  // left
  translate([0 - railDiff/2, 0 - trackHeight/2 + railDepth/2, 0])
    mirror([0, 1, 0])
      railChannel(style);
  // right
  translate([0 + railDiff/2, 0 - trackHeight/2 + railDepth/2, 0])
    mirror([0, 1, 0])
      railChannel(style);
}

module topRoad(style) {
  translate([0, trackHeight/2 - roadDepth/2, 0])
    roadChannel(style);
}

module bottomRoad(style) {
  translate([0, 0 - trackHeight/2 + roadDepth/2, 0])
    mirror([0, 1, 0])
      roadChannel(style);
}

module corners2d(style, width = trackWidth) {
  // don't knock anything off for "square"
  if (style == "square") {
    // nothing
  }

  // knock off all the corners for "round"
  if (style == "round") {
    // top-left
    translate([0 - width/2 + roundBevel/2, trackHeight/2 - roundBevel/2])
      quarterBevelRound(roundBevel + epsilon, "left", "top");

    // bottom-left
    translate([0 - width/2 + roundBevel/2, 0 - trackHeight/2 + roundBevel/2])
      quarterBevelRound(roundBevel + epsilon, "left", "bottom");

    // top-right
    translate([0 + width/2 - roundBevel/2, trackHeight/2 - roundBevel/2])
      quarterBevelRound(roundBevel + epsilon, "right", "top");

    // bottom-right
    translate([0 + width/2 - roundBevel/2, 0 - trackHeight/2 + roundBevel/2])
      quarterBevelRound(roundBevel + epsilon, "right", "bottom");
  }

  // knock off all the corners for "triangle"
  if (style == "triangle") {
    // top-left
    translate([0 - width/2 + triangleBevel/2, trackHeight/2 - triangleBevel/2])
      quarterBevelTriangle(triangleBevel, "left", "top");

    // bottom-left
    translate([0 - width/2 + triangleBevel/2, 0 - trackHeight/2 + triangleBevel/2])
      quarterBevelTriangle(triangleBevel, "left", "bottom");

    // top-right
    translate([0 + width/2 - triangleBevel/2, trackHeight/2 - triangleBevel/2])
      quarterBevelTriangle(triangleBevel, "right", "top");

    // bottom-right
    translate([0 + width/2 - triangleBevel/2, 0 - trackHeight/2 + triangleBevel/2])
      quarterBevelTriangle(triangleBevel, "right", "bottom");
  }
}

module insets2d(style, top, bottom) {
  // track
  if (top == "track") {
    topTrack(style);
  }
  if (bottom == "track") {
    bottomTrack(style);
  }

  // road
  if (top == "road") {
    topRoad(style);
  }
  if (bottom == "road") {
    bottomRoad(style);
  }
}

module roadMarking() {
  minkowski() {
    cube([roadMarkingWidth, roadMarkingLength, roadMarkingHeight - 2*roadMarkingBevel], center = true);
    sphere(roadMarkingBevel);
  }
}

module makeRoadMarkings(length, style, top, bottom, connector1, connector2) {
  // These seem to be the other way around, but that's okay, just so long as we know!
  start = connector2 == "mortise" ? -length/2 + roadMarkingRepeat : -length/2;
  end = connector1 == "mortise" ? length/2 - roadMarkingRepeat - 1 : length/2 - 1;

  // draw a few cubes, with rounded edges
  for(i = [start : roadMarkingRepeat : end]) {
    if (top == "road") {
      translate([0, i + roadMarkingRepeat/2, trackHeight/2 - roadDepth])
        roadMarking();
    }
    if (bottom == "road") {
      translate([0, i + roadMarkingRepeat/2, 0 - roadDepth])
        roadMarking();
    }
  }
}

module track2dBase(style) {
  square([trackWidth, trackHeight], center=true);
}

module track2dDoubleBase(style) {
  square([doubleWidth, trackHeight], center=true);
}

// Variations / Types:
// * https://upload.wikimedia.org/wikipedia/commons/9/9c/Woodtrackprofil.png
//
// We have many variations here, but we'll split it down the style (as in
// that image, "a/b/c/cc") and what we want on the top and bottom
//
// * style:
//   * "square"   - no chamfer anywhere
//   * "round"    - chamfer on all edges (apart from bottom of rails/roads)
//   * "triangle" - slightly more spiky than the others
module track2d(style, top, bottom) {
  difference() {
    track2dBase(style);
    corners2d(style);
    insets2d(style, top, bottom);
  }
}

// Allows an easy extrusion to the correct length for the track base, corners, and insets.
module makeStraight(length) {
  rotate([90, 0, 0])
    linear_extrude(height=length, center=true)
      children();
}

// Allows an easy extrusion to the correct length for the track base, corners, and insets.
module makeCurved(radius, angle = defaultAngle) {
  rotate_extrude(angle = angle, convexity = 2, $fn = 180)
    translate([radius, 0, 0])
      children();
}

module shearAlongY(p) {
  multmatrix([
    [1,p.x/p.y,0,0],
    [0,1,0,0],
    [0,p.z/p.y,1,0]
  ]) children();
}

// Allows an easy shear using the length, and the offset of the track.
module makeShear(length, xOffset) {
  shearAlongY([xOffset / length, 1, 0])
    children();
}
