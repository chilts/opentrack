include <../config.scad>;
use <connector.scad>;
use <track.scad>;

// draw a curved piece of track
module curvedBase(
  radius,
  style = defaultStyle,
  top = "track",
  bottom = "blank",
  connector1 = "tenon",
  connector2 = "mortise",
  angle = defaultAngle
) {
  union() {
    difference() {
      makeCurved(radius, angle = angle)
        track2dBase(style);

      // remove corners
      makeCurved(radius, angle = angle)
        corners2d(style);

      if (connector1 == "mortise") {
        rotate([0, 0, angle])
          translate([radius, 0 - mortiseTotalLength/2, 0])
            mirror([0, 1, 0])
              connector("mortise", top, bottom);
      }

      if (connector2 == "mortise") {
        translate([radius, 0 + mortiseTotalLength/2, 0])
          connector("mortise", top, bottom);
      }
    }

    if (connector1 == "tenon") {
      rotate([0, 0, angle])
        translate([radius, 0 + tenonTotalLength/2, 0])
          mirror([0, 1, 0])
            connector("tenon", top, bottom);
    }

    if (connector2 == "tenon") {
      translate([radius, 0 - tenonTotalLength/2, 0])
        connector("tenon", top, bottom);
    }

  }
}
