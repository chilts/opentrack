include <../config.scad>;
use <track.scad>;

// draw a curved set of insets
module curvedInsets(
  radius,
  style = defaultStyle,
  top = "track",
  bottom = "blank",
  angle = curvedAngle
) {
  difference() {
    // the angle is *always* 45 for both short and long pieces
    rotate_extrude(angle = angle, convexity = 2, $fn = 180)
      translate([radius, 0, 0])
        track2dBase(style);
  }
}
