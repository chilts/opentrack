// Note: offset is usually ±doubleWidthOffsetFromCentre

function tx(i, length, offset) = sin((length/2 + i)/length * 180 + 90) * offset - offset;

// Takes a 2D object, and turns it into a track length.
module makeParallelCurve(length, offset, step = 0.2) {
  union() {
    for(i = [-length/2 : step : length/2]) {
      translate([tx(i, length, offset), i, 0])
        rotate([90, 0, 0])
          linear_extrude(height = step, center = false)
            children();
    }
  }
}
