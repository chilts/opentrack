include <../config.scad>;
use <track.scad>;
use <curved-base.scad>;
use <curved-insets.scad>;

// draw a curved piece of track
module curvedTrack(
  radius,
  style = defaultStyle,
  top = "track",
  bottom = "blank",
  connector1 = "tenon",
  connector2 = "mortise",
  angle = defaultAngle
) {
  union() {
    difference() {
      curvedBase(
        radius,
        style = style,
        top = top,
        bottom = bottom,
        connector1 = connector1,
        connector2 = connector2,
        angle = angle
      );
      makeCurved(radius, angle)
        corners2d(style);
      makeCurved(radius, angle)
        insets2d(style, top, bottom);
    }
  }
}
