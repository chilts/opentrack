include <../config.scad>;
use <connector.scad>;
use <track.scad>;

// draw a regular piece of track
module straightBase(
  length,
  style = defaultStyle,
  top = "track",
  bottom = "blank",
  connector1 = "tenon",
  connector2 = "mortise"
) {
  union() {
    difference() {
      rotate([90, 0, 0])
        linear_extrude(height=length, center=true)
          track2dBase(style);

      // remove corners
      makeStraight(length)
        corners2d(style);

      if (connector1 == "mortise") {
        translate([0, 0 + length/2 - mortiseTotalLength/2, 0])
          mirror([0, 1, 0])
            connector("mortise", top, bottom);
      }

      if (connector2 == "mortise") {
        translate([0, 0 - length/2 + mortiseTotalLength/2, 0])
          connector("mortise", top, bottom);
      }
    }

    if (connector1 == "tenon") {
      translate([0, 0 + length/2 + tenonTotalLength/2, 0])
        mirror([0, 1, 0])
          connector("tenon", top, bottom);
    }

    if (connector2 == "tenon") {
      translate([0, 0 - length/2 - tenonTotalLength/2, 0])
        connector("tenon", , top, bottom);
    }

  }
}

// draw a regular piece of track
module straightDoubleBase(
  length,
  style = defaultStyle,
  top = "track",
  bottom = "blank",
  connector1 = "tenon",
  connector2 = "mortise"
) {
  union() {
    difference() {
      rotate([90, 0, 0])
        linear_extrude(height=length, center=true)
          track2dDoubleBase(style);

      // remove corners
      makeStraight(length)
        corners2d(style, width = doubleWidth);

      if (connector1 == "mortise") {
        translate([-doubleWidthOffsetFromCentre, 0 + length/2 - mortiseTotalLength/2, 0])
          mirror([0, 1, 0])
            connector("mortise", top, bottom);

        translate([doubleWidthOffsetFromCentre, 0 + length/2 - mortiseTotalLength/2, 0])
          mirror([0, 1, 0])
            connector("mortise", top, bottom);
      }

      if (connector2 == "mortise") {
        translate([-doubleWidthOffsetFromCentre, 0 - length/2 + mortiseTotalLength/2, 0])
          connector("mortise", top, bottom);
        translate([doubleWidthOffsetFromCentre, 0 - length/2 + mortiseTotalLength/2, 0])
          connector("mortise", top, bottom);
      }
    }

    if (connector1 == "tenon") {
      translate([-doubleWidthOffsetFromCentre, 0 + length/2 + tenonTotalLength/2, 0])
        mirror([0, 1, 0])
          connector("tenon", top, bottom);
      translate([doubleWidthOffsetFromCentre, 0 + length/2 + tenonTotalLength/2, 0])
        mirror([0, 1, 0])
          connector("tenon", top, bottom);
    }

    if (connector2 == "tenon") {
      translate([-doubleWidthOffsetFromCentre, 0 - length/2 - tenonTotalLength/2, 0])
        connector("tenon", , top, bottom);
      translate([doubleWidthOffsetFromCentre, 0 - length/2 - tenonTotalLength/2, 0])
        connector("tenon", , top, bottom);
    }

  }
}
