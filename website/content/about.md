{
  "type": "page",
  "published": "2020-03-27T22:34:17.598Z"
}

---

# About OpenTrack #

OpenTrack is a huge library of STL parts you can 3D print for use with your
toy wooden train track. It is compatible with lots of other makes and
manufacturers including:

* Thomas
* Brio
* Melissa and Doug
* Ikea
* Asda George
* other generic track

It was originally built by [Andrew Chilton](https://chilts.org/) during the
Covid19 epidemic in 2020 whilst isolated at home with his family, including at
least one child obsessed with trains and his wooden train track.

Built in [OpenSCAD](http://www.openscad.org/), OpenTrack is both open to
contributions and to download, print, and play with as your desire.

Everything in this repository, including code, generated/built files, website,
content and images are licensed under
[Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](http://creativecommons.org/licenses/by-sa/4.0/).
