{
  "type": "post",
  "title": "Welcome to Open Track",
  "published": "2020-03-27T20:43:02.997Z"
}
---

Picture it now. Your young kids are playing with their wooden train
tracks. It's growing larger and larger, spewing out of the bedroom, down the
hallway and into the lounge. But then tragedy strikes!

You don't have a converter piece! Arrrgh! The one piece you really need right
now. But here comes a hero ...

Welcome to [OpenTrack.xyz](https://OpenTrack.xyz)!
